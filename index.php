<html>
    <head>
        <title>home page</title>
        <link rel="stylesheet" href="css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/animate/animate.min.css">
        <link rel="stylesheet" href="fontawesome-free/css/all.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="css/bootstrap4/js/bootstrap.min.js"></script>
        <style>
            #about,.newsimg,.form1pg, .form2pg, .form3pg, .form4pg{
                display:none;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="content row justify-content-center">
                <div class="row div2 mr-5 ml-5 mb-3">
                    <div class="col-md-12 mt-1 mb-3  top text-center rounded">
                        <img src="images/logo.png" class="rounded" width="5%" >
                        NAIVASHA GIRLS SECONDARY SCHOOL 
                        <div class="motto"><i>School Motto : Diligence For Excellence</i></div>
                    </div> 
                </div>
            </div>
            <div class="content row justify-content-end">
                <nav class=" nav navbar navbar-light  navbar-expand-lg ">
                    <button class="navbar-toggler"
                    type="button" data-toggle="collapse"
                    data-target="#navbarContent" 
                    aria-expanded="false">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active mr-3">
                                <button class="nav-link btn btn-success " id="homebtn">
                                    HOME
                                </button>
                            </li>

                            <li class="nav-item mr-3">
                                <button class="nav-link btn btn-success" id="newsbtn">
                                    NEWS AND EVENTS
                                </button>
                                
                            </li>
                            
                            <li class="nav-item mr-3">
                                <button class="nav-link btn btn-success" id="aboutbtn">
                                    ABOUT US
                                </button>
                            </li>
                            
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="row mt-5 justify-content-center">
                
                <div class="col-md-10 mt-1" id="content">
                    <div class="row" id="homeimg">
                        <div class="col-md-12 mb-3   image1">
                            <div  class="texts   text-center mt-5 mb-5 pt-5 pb-5">
                                <b>WE BELIEVE IN DIVERSITY AND THEREFORE ADMISSION IS OPEN TO LADIES COUNTRYWIDE</b>
                            </div>
                        </div> 
                    
                    </div>
                    <?php include "ABOUT.PHP";
                    include "news.php"; 
                    include "form1pg.php";?>
                    <div class="col-md-12 text-center">
                        <i class="fa fa-angle-down animated infinite flash"></i>
                    </div>
                </div>    
            
            </div>
            <div class="row  mt-5  mb-5 pb-5 pt-5 pb-5 justify-content-center">
                
                <div  class="  col-md-3 boss mr-5 ml-1 pt-0   pl-0 text-center">
                    <div class="triangle1"></div>
                    <h1>School Mission </h1>
                        <p> One time, in the state of Jin, a thief stole a huge beautiful bell.
                        He carried the bell off on his back, as it was too heavy to carry any other way.
                        Each time he took a step, the bell made a ringing noise.</p>
                </div>
                <div  class=" col-md-3 boss mr-1 ml-1 pt-0 pl-0 text-center">
                    <div class="triangle2"></div>
                        <h1>School Vission</h1>
                            <p> One time, in the state of Jin, a thief stole a huge beautiful bell.
                            He carried the bell off on his back, as it was too heavy to carry any other way.
                            Each time he took a step, the bell made a ringing noise.</p>
                </div>
                <div  class=" col-md-3 boss mr-5 ml-3 pt-0 pl-0 text-center">
                    <div class="triangle3"></div>
                    <h1>Core Value</h1>
                        <p> One time, in the state of Jin, a thief stole a huge beautiful bell.
                        He carried the bell off on his back, as it was too heavy to carry any other way.
                        Each time he took a step, the bell made a ringing noise.</p>
                </div>
            </div>
            <div class="row sub mt-5 justify-content-center mb-5 pb-5">
                <div  class="  col-md-12   text-center mt-5 ">
                    <!-- <i class="fa fa-user-tie mt-5"></i><br> -->
                </div>
                <div  class="  col-md-3 boss1 pt-1   text-center pl-0 mr-5 mt-5">
                    <img class="img rounded-circle" src="images/me.jpg"  width="40%" height="85px" >
                    <b class="vision1"><h3>School Principle</h3></b>
                        <p class="myText pt-3 pl-3"> One time, in the state of Jin, a thief stole a huge beautiful bell.
                        He carried the bell off on his back, as it was too heavy to carry any other way.
                        Each time he took a step, the bell made a ringing noise.</p>
                </div>
                <div  class=" col-md-3 boss2 pt-3  text-center pl-0 mr-5 mt-5">
                    <img class="img rounded-circle" src="images/mg.jpg"  width="35%" height="80px">
                        <b class="vision1"><h3>Head Girl<h3></b>
                            <p class="myText pt-3 pl-3"> One time, in the state of Jin, a thief stole a huge beautiful bell.
                            He carried the bell off on his back, as it was too heavy to carry any other way.
                            Each time he took a step, the bell made a ringing noise.</p>
                </div>
                <div  class=" col-md-3 boss3 pt-3 text-center pl-0 mt-5">
                    
                    <img class="img rounded-circle" src="images/IMG-20161123-WA0004.jpg"  width="35%" height="90px" >
                        <b class="vision1"><h3>Discipline Master</h3></b>
                            <p class="myText pt-3 pl-3"> One time, in the state of Jin, a thief stole a huge beautiful bell.
                            He carried the bell off on his back, as it was too heavy to carry any other way.
                            Each time he took a step, the bell made a ringing noise.</p>
                </div>
            </div>
                     
            
            <div class="footer row justify-content-center  pb-3">
               
                <div class=" card-footer col-md-4">
                    Success is the result of perfection,hard work, learning from failure, layalty and persistence
                    ~Colin Powell
                </div>
            </div>
        </div>   
    </body>
    <script src="js/formscript.js"></script>
</html>