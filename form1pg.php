<html>
    
        <div class="form1pg">

            <div class="row justify-content-center animated bounceInDown" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center ">
                    <h4>Form One Activities</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center ">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated bounceInLeft" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Important Dates</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated  bounceInUp" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form One School Fees</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>
        </div>


        <div class="form2pg">

            <div class="row justify-content-center animated flash" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form Two Activities</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated flash" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Important Dates</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated  flash" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form Two School Fees</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>
        </div>


        <div class="form3pg">
            <div class="row justify-content-center animated shake" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form Three Activities</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>
            
            <div class="row justify-content-center animated shake" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Important Dates</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated  shake" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form Three School Fees</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>
        </div>


        <div class="form4pg">
            <div class="row justify-content-center animated rubberBand" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form Four Activities</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated rubberBand" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Important Dates</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>

            <div class="row justify-content-center animated  rubberBand" >
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    <h4>Form Four School Fees</h4>
                </div>
                <div class="col-md-4     mt-2 mb-2 mr-2 text-center">
                    Libero congue maximus orci eu convallis 
                    orci fusce nunc lacus dignissim ac
                    gravida id ultricies id mi nulla eget vulputate sem id 
                    <!-- placerat velit aliquam arcu
                    justo tristique sed neque vitae aliquet mattis libero proin consequat metus sit amet -->
                </div>
            </div>
        </div>
   
</html>